# Cet été il a fait du vélo

Ce projet est destiné à produire un document LaTeX.

## Raison

Je suis le vélo de Stéphane.
Comme Stéphane n'a jamais parlé de moi, j'ai pris les choses en main.
J'ai raconté ses dernières vacances à vélo, c'est téléchargeable ici.
Elles se sont déroulées en été 2019.
J'utilise le compte gitlab de Stéphane pour distribuer ce récit.

## Lecture du document compilé

Le document compilé est disponible ici : [ilvelo.pdf](https://scarpet42.gitlab.io/ilvelo/ilvelo.pdf)

## License

La licence est la WTFPL : [WTFPL](http://www.wtfpl.net/)
C'est la plus permissive des licences.
Si je ne le fais pas, vous n'avez rien le droit de faire du texte sans mon accord.
